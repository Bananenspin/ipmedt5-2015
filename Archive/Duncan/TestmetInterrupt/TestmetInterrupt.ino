#include <Wire.h>
#include "LedControl.h" //  Include de library
#include "Adafruit_MCP23017.h"
#include "LedControl.h"

Adafruit_MCP23017 mcp;
LedControl lc=LedControl(11,10,9,1); // selecteer pinnen 11,10,9 en geef aan dat er 1 chip wordt gebruikt

byte arduinoInterrupt=1;

int setLed = 0;
boolean gameOn = false;
volatile boolean awakenByInterrupt = false;
int players = 1;
int score = 0;
int tijd = 0;
int difficulty = 5;

char ledState[] = { 
   LOW,LOW,LOW,LOW,
   LOW,LOW,LOW,LOW,
   LOW,LOW,LOW,LOW,
   LOW,LOW,LOW,LOW
};

void setup(){
    mcp.begin();      // use default address 0
  
  // We mirror INTA and INTB, so that only one line is required between MCP and Arduino for int reporting
  // The INTA/B will not be Floating 
  // INTs will be signaled with a LOW
  mcp.setupInterrupts(true,false,LOW);

  // configuration for a button on port A
  // interrupt will triger when the pin is taken to ground by a pushbutton
  for(int start = 0; start < 16; start++)
  {
     mcp.pinMode(start, INPUT); 
     mcp.pullUp(start, HIGH); 
  }
}

void setUpInterrupt(){
  for(int start = 0; start < 16; start++)
  {
     mcp.setupInterruptPin(start,FALLING); 
  }
}

// The int handler will just signal that the int has happen
// we will do the work from the main loop.
void intCallBack(){
  awakenByInterrupt=true;
}

void handleInterrupt(){
  
  // Get more information from the MCP from the INT
  uint8_t pin=mcp.getLastInterruptPin();
  // uint8_t val=mcp.getLastInterruptPinValue();
  
 /*
 * Een plus is van af de knop gezien -1, 0, 1, -4, +4
 * Volledige plussen kunnen plaatsvinden op 6,5,10,9
 *
 *  ____________________
 * | 00 | 01 | 02 | 03 |  
 * ---------------------
 * | 04 | 05 | 06 | 07 |  
 * ---------------------
 * | 08 | 09 | 10 | 11 |  
 * ---------------------
 * | 12 | 13 | 14 | 15 |  
 * ---------------------
 */
  
  // doe iets met LEDS komt hier
  if(pin == 0){
    ledState[0] = !ledState[0];
    ledState[1] = !ledState[1];
    ledState[4] = !ledState[4];
  } else if(pin == 1){
     ledState[0] = !ledState[0];
     ledState[1] = !ledState[1];
     ledState[2] = !ledState[2];
     ledState[5] = !ledState[5];
  } else if(pin == 2){
     ledState[3] = !ledState[3];
     ledState[1] = !ledState[1];
     ledState[2] = !ledState[2];
     ledState[6] = !ledState[6];
  } else if(pin == 3){
    ledState[3] = !ledState[3];
    ledState[2] = !ledState[2];
    ledState[7] = !ledState[7];
  } else if(pin == 4){
    ledState[0] = !ledState[0];
    ledState[5] = !ledState[5];
    ledState[4] = !ledState[4];
    ledState[8] = !ledState[8];
  } else if(pin == 5) {
     ledState[5] = !ledState[5];
     ledState[6] = !ledState[6];
     ledState[4] = !ledState[4];
     ledState[1] = !ledState[1];
     ledState[9] = !ledState[9];
   } else if(pin == 6) {
     ledState[5] = !ledState[5];
     ledState[6] = !ledState[6];
     ledState[7] = !ledState[7];
     ledState[2] = !ledState[2];
     ledState[10] = !ledState[10];
   }  else if(pin == 7) {
     ledState[3] = !ledState[3];
     ledState[6] = !ledState[6];
     ledState[7] = !ledState[7];
     ledState[11] = !ledState[11];
  } else if(pin == 8) {
     ledState[8] = !ledState[8];
     ledState[4] = !ledState[4];
     ledState[12] = !ledState[12];
     ledState[9] = !ledState[9];
  } else if(pin == 9) {
     ledState[5] = !ledState[5];
     ledState[9] = !ledState[9];
     ledState[8] = !ledState[8];
     ledState[13] = !ledState[13];
     ledState[10] = !ledState[10];
   } else if(pin == 10) {
     ledState[14] = !ledState[14];
     ledState[6] = !ledState[6];
     ledState[11] = !ledState[11];
     ledState[9] = !ledState[9];
     ledState[10] = !ledState[10];     
   } else if(pin == 11){
     ledState[7] = !ledState[7];
     ledState[10] = !ledState[10];
     ledState[11] = !ledState[11];
     ledState[15] = !ledState[15];
  } else if(pin == 12){
    ledState[8] = !ledState[8];
    ledState[12] = !ledState[12];
    ledState[13] = !ledState[13];
  } else if(pin == 13){
    ledState[9] = !ledState[9];
    ledState[12] = !ledState[12];
    ledState[13] = !ledState[13];
    ledState[14] = !ledState[14];
  } else if(pin == 14){
    ledState[10] = !ledState[7];
    ledState[13] = !ledState[13];
    ledState[14] = !ledState[14];
    ledState[15] = !ledState[15];
  } else if(pin == 15){
    ledState[11] = !ledState[11];
    ledState[14] = !ledState[14];
    ledState[15] = !ledState[15];
  }

  // we have to wait for the interrupt condition to finish
  // otherwise we might go to sleep with an ongoing condition and never wake up again.
  // as, an action is required to clear the INT flag, and allow it to trigger again.
  // see datasheet for datails.
  while( ! (mcp.digitalRead(0) && mcp.digitalRead(1) && mcp.digitalRead(3) && mcp.digitalRead(4) && mcp.digitalRead(5) && mcp.digitalRead(6) && mcp.digitalRead(7) && mcp.digitalRead(8) && mcp.digitalRead(9) && mcp.digitalRead(10) && mcp.digitalRead(11) && mcp.digitalRead(12) && mcp.digitalRead(13) && mcp.digitalRead(14) && mcp.digitalRead(15)));
  // and clean queued INT signal
  zetLed();
}

void zetLed(){
   setLed = 0;
   for(int y = 0; y < 4; y++)
   {
     for(int x = 0; x < 4; x++)
     {
       //  void setLed(int addr, int row, int col, boolean state);
        lc.setLed(0,y,x, ledState[setLed]);
        setLed++;
      }
   }
  
  // and clean queued INT signal
  cleanInterrupts();
}

// handy for interrupts triggered by buttons
// normally signal a few due to bouncing issues
void cleanInterrupts(){
  EIFR=0x01;
  awakenByInterrupt=false;
}  

void showPlayer(int p)
{
  if (p == 1)
  {
    lc.setChar(0, 0, 'p', false);
    lc.setChar(0, 1, '1', false);
  } else if (p == 2) {
    lc.setChar(0, 4, 'p', false);
    lc.setChar(0, 5, '2', false);
  }
}

void showTimeText(int p)
{
  if (p == 1)
  {
    lc.setChar(1, 0, 't', false);
    lc.setChar(1, 1, 'i', false);
    lc.setChar(1, 2, 'm', false);
    lc.setChar(1, 3, 'e', false);
  } else if (p == 2) {
    lc.setChar(1, 4, 't', false);
    lc.setChar(1, 5, 'i', false);
    lc.setChar(1, 6, 'm', false);
    lc.setChar(1, 7, 'e', false);
  }
}

void showTime(int p)
{
  if (p == 1)
  {
   
  }
}

void showScore(int p, int score)
{
  int s1 = (score / 1000);
  int s2 = ((score / 100) % 10);
  int s3 = ((score / 10) % 10);
  int s4 = (score % 10);

  if (p == 1)
  {
    lc.setDigit(2, 0, s1, false);
    lc.setDigit(2, 1, s2, false);
    lc.setDigit(2, 2, s3, false);
    lc.setDigit(2, 3, s4, false);
  } else if (p == 2) {
    lc.setDigit(2, 4, s1, false);
    lc.setDigit(2, 5, s2, false);
    lc.setDigit(2, 6, s3, false);
    lc.setDigit(2, 7, s4, false);
  }
}

void loop(){
  if(gameOn)
  {
  // enable interrupts before going to sleep/wait
  // And we setup a callback for the arduino INT handler.
  attachInterrupt(arduinoInterrupt,intCallBack,FALLING);
  
  // Simulate a deep sleep
  while(!awakenByInterrupt){
   showPlayer(players);
   showTimeText(tijd);
   showTime(players);
   showScore(players, score);
   
  } // Hier komt scorebord code
  
  // disable interrupts while handling them.
  detachInterrupt(arduinoInterrupt);
  
  if(awakenByInterrupt) handleInterrupt();
  } else { startGame(); }
}
