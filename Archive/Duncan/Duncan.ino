#include "LedControl.h" //  Include de library
LedControl lc=LedControl(11,10,9,1); // selecteer pinnen 11,10,9 en geef aan dat er 1 chip wordt gebruikt
 
// pin 11 is connected to the MAX7219 pin 1
// pin 10 is connected to the CLK pin 13
// pin 9 is connected to LOAD pin 12

void setup() {
  lc.shutdown(0,false);// turn off power saving, enables display
  lc.setIntensity(0,8);// sets brightness (0~15 possible values)
  lc.clearDisplay(0);// clear screen

  
  
}

void allesAan(){
  for(int y = 0; y < 4; y++)
  {
    for(int x = 0; x < 4; x++)
    {
     //  void setLed(int addr, int row, int col, boolean state);
      lc.setLed(0,y,x, true);
      delay(100);
    }
  }
}

void allesUit(){
  for(int y = 0; y < 4; y++)
  {
    for(int x = 0; x < 4; x++)
    {
     //  void setLed(int addr, int row, int col, boolean state);
      lc.setLed(0,y,x, false);
      delay(100);
    }
  }
}

void loop() {
  allesAan();
  allesUit();
}
