#include <Wire.h>
#include "LedControl.h" //  Include de library
#include "Adafruit_MCP23017.h"
#include "LedControl.h"

Adafruit_MCP23017 mcp;
LedControl lc=LedControl(11,10,9,1); // selecteer pinnen 11,10,9 en geef aan dat er 1 chip wordt gebruikt

char ledState[] = { 
   LOW,LOW,LOW,LOW,
   LOW,LOW,LOW,LOW,
   LOW,LOW,LOW,LOW,
   LOW,LOW,LOW,LOW
};

char newState[] = { 
   LOW,LOW,LOW,LOW,
   LOW,LOW,LOW,LOW,
   LOW,LOW,LOW,LOW,
   LOW,LOW,LOW,LOW
};

int setLed = 0;
 
// pin 11 is connected to the MAX7219 pin 1
// pin 10 is connected to the CLK pin 13
// pin 9 is connected to LOAD pin 12

void setup() {
  lc.shutdown(0,false);// turn off power saving, enables display
  lc.setIntensity(0,8);// sets brightness (0~15 possible values)
  lc.clearDisplay(0);// clear screen  
  
  mcp.begin();
  for(int start = 0; start < 16; start++)
  {
     mcp.pinMode(start, INPUT); 
     mcp.pullUp(start, HIGH); 
  }
}

void leesButton(){
  for(int lees = 0; lees < 16; lees++)
   {
     newState[lees] = mcp.digitalRead(lees);      
   }
}

void zetLed(){
 setLed = 0;
 for(int y = 0; y < 4; y++)
   {
     for(int x = 0; x < 4; x++)
     {
       //  void setLed(int addr, int row, int col, boolean state);
        lc.setLed(0,y,x, ledState[setLed]);
        setLed++;
      }
   }
}

void algoritme(){
 
 /*
 * Een plus is van af de knop gezien -1, 0, 1, -4, +4
 * Volledige plussen kunnen plaatsvinden op 6,5,10,9
 *
 *  ____________________
 * | 00 | 01 | 02 | 03 |  
 * ---------------------
 * | 04 | 05 | 06 | 07 |  
 * ---------------------
 * | 08 | 09 | 10 | 11 |  
 * ---------------------
 * | 12 | 13 | 14 | 15 |  
 * ---------------------
 */
 
   if(newState[5] != ledState[5])
   {
     ledState[5] = !ledState[5];
     ledState[6] = !ledState[6];
     ledState[4] = !ledState[4];
     ledState[1] = !ledState[1];
     ledState[9] = !ledState[9];
   } else if(newState[6] != ledState[6]) {
     ledState[5] = !ledState[5];
     ledState[6] = !ledState[6];
     ledState[7] = !ledState[7];
     ledState[2] = !ledState[2];
     ledState[10] = !ledState[10];
   } else if(newState[9] != ledState[9]) {
     ledState[5] = !ledState[5];
     ledState[9] = !ledState[9];
     ledState[8] = !ledState[8];
     ledState[13] = !ledState[13];
     ledState[10] = !ledState[10];
   } else if(newState[10] != ledState[10]) {
     ledState[14] = !ledState[14];
     ledState[6] = !ledState[6];
     ledState[11] = !ledState[11];
     ledState[9] = !ledState[9];
     ledState[10] = !ledState[10];     
   }
}

void loop() {
   leesButton();
   zetLed();   
}
