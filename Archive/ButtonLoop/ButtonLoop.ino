#include <Wire.h> 
#include "Adafruit_MCP23017.h"
#include "LedControl.h"

Adafruit_MCP23017 mcp; 
int inputbutton = 0;
int LedPin = 13;

void setup() {

  mcp.begin(); 
  for (int i =0; i <16; i++){
    mcp.pinMode(i, INPUT); 
    mcp.pullUp(i, HIGH);
  }
  pinMode(LedPin, OUTPUT);

}


void loop() {
  // put your main code here, to run repeatedly:
for (int i = 0; i<16; i++){

digitalWrite(LedPin, mcp.digitalRead(i));

}}
