#include "LedControl.h" //  need the library
LedControl lc = LedControl(12, 11, 10, 2); //

// pin 12 is connected to the MAX7219 pin 1
// pin 11 is connected to the CLK pin 13
// pin 10 is connected to LOAD pin 12
// 1 as we are only using 1 MAX7219

int score = 0;
int s1, s2, s3, s4;

void setup() {
  lc.shutdown(0, false); // turn off power saving, enables display
  lc.setIntensity(0, 8); // sets brightness (0~15 possible values)
  lc.shutdown(1, false); // turn off power saving, enables display
  lc.setIntensity(1, 8); // sets brightness (0~15 possible values)
  lc.clearDisplay(0);// clear screen
  lc.clearDisplay(1);// clear screen
  //lc.setChar(1,0,'a', false);
  //lc.setChar(1,1,'b', false);

}

void loop() {


  /*
  for (int a=0; a<16; a++)
  {
    lc.setDigit(0,a,a,true);
    delay(100);
  }
  for (int a=0; a<16; a++)
  {
    lc.setDigit(0,a,8,1);
    delay(100);
  }
  for (int a=0; a<16; a++)
  {
    lc.setDigit(0,a,0,false);
    delay(100);
  }
  for (int a=0; a<16; a++)
  {
    lc.setChar(0,a,' ',false);
    delay(100);
  }
  for (int a=0; a<16; a++)
  {
    lc.setChar(0,a,'-',false);
    delay(100);
  }
  for (int a=0; a<16; a++)
  {
    lc.setChar(0,a,' ',false);
    delay(100);
  }*/

}

void showPlayer(int p)
{
  if (p == 1)
  {
    lc.setChar(0, 0, 'p', false);
    lc.setChar(0, 1, '1', false);
  } else if (p == 2) {
    lc.setChar(0, 4, 'p', false);
    lc.setChar(0, 5, '2', false);
  }
}

void showTimeText(int p)
{
  if (p == 1)
  {
    lc.setChar(1, 0, 't', false);
    lc.setChar(1, 1, 'i', false);
    lc.setChar(1, 2, 'm', false);
    lc.setChar(1, 3, 'e', false);
  } else if (p == 2) {
    lc.setChar(1, 4, 't', false);
    lc.setChar(1, 5, 'i', false);
    lc.setChar(1, 6, 'm', false);
    lc.setChar(1, 7, 'e', false);
  }
}

void showTime(int p)
{
  if (p == 1)
  {

  }
}

void showScore(int p, int score)
{
  s1 = (score / 1000);
  s2 = ((score / 100) % 10);
  s3 = ((score / 10) % 10);
  s4 = (score % 10);

  if (p == 1)
  {
    lc.setChar(2, 0, s1, false);
    lc.setChar(2, 1, s2, false);
    lc.setChar(2, 2, s3, false);
    lc.setChar(2, 3, s4, false);
  } else if (p == 2) {
    lc.setChar(2, 4, s1, false);
    lc.setChar(2, 5, s2, false);
    lc.setChar(2, 6, s3, false);
    lc.setChar(2, 7, s4, false);
  }
}
