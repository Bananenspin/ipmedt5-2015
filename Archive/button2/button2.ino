#include <Wire.h>
#include "Adafruit_MCP23017.h"
#include "LedControl.h"
LedControl lc = LedControl(11,10,9,1);

// Basic pin reading and pullup test for the MCP23017 I/O expander
// public domain!

// Connect pin #12 of the expander to Analog 5 (i2c clock)
// Connect pin #13 of the expander to Analog 4 (i2c data)
// Connect pins #15, 16 and 17 of the expander to ground (address selection)
// Connect pin #9 of the expander to 5V (power)
// Connect pin #10 of the expander to ground (common ground)
// Connect pin #18 through a ~10kohm resistor to 5V (reset pin, active low)

// Input #0 is on pin 21 so connect a button or switch from there to ground

Adafruit_MCP23017 mcp;
  
void setup() {  
  lc.shutdown(0,false); 
  lc.setIntensity(0,8); 
  lc.clearDisplay(0); 
  mcp.begin();      // use default address 0

  mcp.pinMode(0, INPUT);
  mcp.pullUp(0, HIGH);  // turn on a 100K pullup internally

 
  mcp.begin();
  mcp.pinMode(1, INPUT);
 mcp.pullUp(1, HIGH);  

  mcp.begin();  
  mcp.pinMode(2, INPUT); 
  mcp.pullUp(2, HIGH);   
 
   
  mcp.begin();  
  mcp.pinMode(3, INPUT);
 mcp.pullUp(3, HIGH); 
 
  
  mcp.begin();  
  mcp.pinMode(4, INPUT);
 mcp.pullUp(4, HIGH); 

  
  mcp.begin();  
 mcp.pinMode(5, INPUT);
 mcp.pullUp(5, HIGH); 
 
  
  mcp.begin();  
mcp.pinMode(6, INPUT);
 mcp.pullUp(6, HIGH); 
 
  
  mcp.begin();  
 mcp.pinMode(7, INPUT);
 mcp.pullUp(7, HIGH);

  
  mcp.begin();  
mcp.pinMode(8, INPUT);
 mcp.pullUp(8, HIGH);

  
  mcp.begin();  
mcp.pinMode(9, INPUT);
 mcp.pullUp(9, HIGH); 

  
  mcp.begin();  
 mcp.pinMode(10, INPUT);
 mcp.pullUp(10, HIGH); 

  
  mcp.begin();  
mcp.pinMode(11, INPUT);
 mcp.pullUp(11, HIGH); 

  
  mcp.begin();  
mcp.pinMode(12, INPUT);
 mcp.pullUp(12, HIGH); 

   
  mcp.begin();  
  mcp.pinMode(13, INPUT);
 mcp.pullUp(13, HIGH); 

  
  mcp.begin();  
mcp.pinMode(14, INPUT);
 mcp.pullUp(14, HIGH); 

  
  mcp.begin();  
mcp.pinMode(15, INPUT);
mcp.pullUp(15, HIGH);

 

}



void loop() {
  // The LED will 'echo' the button
  lc.setLed(0,1,1, mcp.digitalRead(0));
  lc.setLed(0,2,2, mcp.digitalRead(1));
  lc.setLed(0,3,3, mcp.digitalRead(2));
  lc.setLed(0,4,4, mcp.digitalRead(3));
  lc.setLed(0,5,5, mcp.digitalRead(4));
  lc.setLed(0,6,6, mcp.digitalRead(5));
  lc.setLed(0,7,7, mcp.digitalRead(6));
  lc.setLed(0,8,8, mcp.digitalRead(7));
  lc.setLed(0,9,9, mcp.digitalRead(8));
  lc.setLed(0,10,10, mcp.digitalRead(9));
  lc.setLed(0,11,11, mcp.digitalRead(10));
  lc.setLed(0,12,12, mcp.digitalRead(11));
  lc.setLed(0,13,13, mcp.digitalRead(12));
  lc.setLed(0,14,14, mcp.digitalRead(13));
  lc.setLed(0,15,15, mcp.digitalRead(14));
  lc.setLed(0,16,16, mcp.digitalRead(15));
}
