#include <Wire.h> // I2C library voor de mcp23017 chip
#include <binary.h> // Library om binary te snappen en door te geven 
#include <EEPROM.h> // Library voor het lezen en schrijven in het EEPROM geheugen
#include "LedControl.h" //  De led control library 
#include "CountUpDownTimer.h" //  Timer library
#include "Adafruit_MCP23017.h" //  I2C extender library de i2c chip
#include "noten.h" // Library om alle noten om te zetten in tonen

#define OCTAVE_OFFSET 0 // Voor hoge tonen
#define TONE_PIN 13 // Pin voor de speakers

Adafruit_MCP23017 mcp;
LedControl lc = LedControl(12, 11, 10, 4); // selecteer pinnen 12,11,10 en geef aan dat er 4 chips worden gebruikt

boolean gameOn = false, clearScreenState = false, dpaan = true, doublePoints = false, blueWon = true, greenWon = true; // Booleans voor het schoonmaken van het scherm, het spel starten, mp win, dubbele punt en double points
int playerTurn = 1, players = 0; // hoeveel spelers en wie is er aan de beurt
int buttonRead = 99; // int voor het kijken of er een knop wordt ingedrukt
const int levels = 5; // constant integer voor hoeveel levels er zijn
int won = 0, level = 0, ledSteps = 0; // Integer tegen gewonnen, welk level en hoeveel stappen

// ints voor score gedeelte
int score = 0, s1, s2, s3, s4;

// ints voor tijd gedeelte
int showSec, showMin;
int leftTimeSec, rightTimeSec, leftTimeMin, rightTimeMin;

int counter = 0; // integer voor het op pauze zetten

// Array voor alle lampjes
int ledGreen[16] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

int ledBlue[16] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

// Vertel dat de timer dat hij moet optellen
CountUpDownTimer T(UP);

// De arrays van elk level, de waardes staan voor de corresponderende leds en 99 wordt gebruikt om de array op te vullen want deze moet 16 waarden hebben
int gameLevels[levels][16] = {
  {7, 5, 15, 13, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99},
  {7, 6, 5, 4, 0, 12, 8, 9, 10, 11, 15, 3, 99, 99, 99, 99},
  {6, 5, 3, 2, 1, 0, 15, 14, 13, 12, 10, 9, 99, 99, 99, 99},
  {7, 4, 11, 8, 2, 1, 14, 13, 99, 99, 99, 99, 99, 99, 99, 99},
  {7, 6, 4, 3, 1, 14, 13, 9, 8, 99, 99, 99, 99, 99, 99, 99}
};

// Animatie voor het winnen van het level
char fallingLedsArray[] = {
  B00000001, B00000000, B00000011, B00000000, B00000111, B00000000, B00001111, B00000000,
  B00011111, B00000000, B00111110, B00000000, B01111100, B00000000, B11111000, B00000000,
  B11110000, B00000001, B11100000, B00000011, B11000000, B00000111, B10000000, B00001111,
  B00000000, B00011111, B00000000, B00111110, B00000000, B01111100, B00000000, B11111000,
  B00000000, B11110000, B00000000, B11100000, B00000000, B11000000, B00000000, B10000000,
  B00000000, B00000000
};

char starLedsArray[] = {
  B11111110, B11111111, B11101100, B11111111, B11001000, B11111110, B10000000, B11101100,
  B00000000, B11001000, B00000000, B10000000, B00000000, B00000000
};

// Voor de muziek, de noten die gespeeld kunnen worden
int notes[] = { 0,
                NOTE_C4, NOTE_CS4, NOTE_D4, NOTE_DS4, NOTE_E4, NOTE_F4, NOTE_FS4, NOTE_G4, NOTE_GS4, NOTE_A4, NOTE_AS4, NOTE_B4,
                NOTE_C5, NOTE_CS5, NOTE_D5, NOTE_DS5, NOTE_E5, NOTE_F5, NOTE_FS5, NOTE_G5, NOTE_GS5, NOTE_A5, NOTE_AS5, NOTE_B5,
                NOTE_C6, NOTE_CS6, NOTE_D6, NOTE_DS6, NOTE_E6, NOTE_F6, NOTE_FS6, NOTE_G6, NOTE_GS6, NOTE_A6, NOTE_AS6, NOTE_B6,
                NOTE_C7, NOTE_CS7, NOTE_D7, NOTE_DS7, NOTE_E7, NOTE_F7, NOTE_FS7, NOTE_G7, NOTE_GS7, NOTE_A7, NOTE_AS7, NOTE_B7
              };

char *song;

int eepromreader = 0, eepromreadermax = 0; // Lezers voor het geheugen
int seconds, minutes; // integers voor het opslaan van de seconden en minuten
boolean stepScore = false, secondScore = false, minuteScore = false; // BOoleans om te kijken of iets gewonnen is
int oldScore[16]; // array om de score in op te slaan
boolean pauseState = false;

void setup() { // Functie van de arduino die alles opstart
  int segmentBright = 15;
  int ledBright = 8;

  lc.shutdown(0, false); // Zet power saving uit en zet display aan
  lc.shutdown(1, false); // Zet power saving uit en zet display aan
  lc.shutdown(2, false); // Zet power saving uit en zet display aan
  lc.shutdown(3, false); // Zet power saving uit en zet display aan

  lc.setIntensity(0, segmentBright); // Zet helderheid van 0 tot 15
  lc.setIntensity(1, segmentBright); // Zet helderheid van 0 tot 15
  lc.setIntensity(2, segmentBright); // Zet helderheid van 0 tot 15
  lc.setIntensity(3, ledBright); // Zet helderheid van 0 tot 15

  lc.clearDisplay(0);// Leeg scherm
  lc.clearDisplay(1);// Leeg scherm
  lc.clearDisplay(2);// Leeg scherm
  lc.clearDisplay(3);// Leeg scherm

  mcp.begin();      // use default address 0

  for (int start = 0; start < 16; start++)
  {
    mcp.pinMode(start, INPUT);
    mcp.pullUp(start, HIGH);
  }

  T.StartTimer();
}

void readButtons() { // functie om de LED's om te zetten. Author: Duncan

  /*
  * Een plus is van af de knop gezien -1, 0, 1, -4, +4
  *
  * lc.setColumn(3,1 ,B11111111); // rood beneden
  * lc.setColumn(3,2 ,B11111111); // blauw beneden
  * lc.setColumn(3,3 ,B11111111); // groen beneden
  * lc.setColumn(3,4 ,B11111111); // rood boven
  * lc.setColumn(3,5 ,B11110111); // blauw ( met 1 groen op b5)
  * lc.setColumn(3,6 ,B11111111); // groen ( met 1 blauw op b5)
  *
  *   _______________________________________________
  *  | B00000001 | B00000010 | B00000100 | B00001000 |
  *  -------------------------------------------------
  *  | B00010000 | B00100000 | B01000000 | B10000000 |
  *  ------------------------------------------------
  *  | B00000001 | B00000010 | B00000100 | B00001000 |
  *  -------------------------------------------------
  *  | B00010000 | B00100000 | B01000000 | B10000000 |
  *  _________________________________________________
  */

  if (mcp.digitalRead(0) == 0) { // Als knop 0 wordt ingedukt
    if (playerTurn == 1) { // Als speler 1 aan de beurt is // Als speler 1 aan de beurt is
      ledGreen[7] = !ledGreen[7]; // Verander de staat van LED 8
      ledGreen[6] = !ledGreen[6]; // Verander de staat van LED 7
      ledGreen[3] = !ledGreen[3]; // Verander de staat van LED 4
      ledGreen[11] = !ledGreen[11]; // Verander de staat van LED 12
      ledBlue[4] = !ledBlue[4]; // Verander de staat van LED 5
    } else if (playerTurn == 2) { // Als speler 2 aan de beurt is
      ledBlue[7] = !ledBlue[7]; // Verander de staat van LED 8
      ledBlue[6] = !ledBlue[6]; // Verander de staat van LED 7
      ledBlue[3] = !ledBlue[3]; // Verander de staat van LED 4
      ledBlue[11] = !ledBlue[11]; // Verander de staat van LED 12
      ledGreen[4] = !ledGreen[4]; // Verander de staat van LED 5
    }
    buttonRead = 0; // Zet buttonRead op 0, waardoor er een stap gezet wordt
  }

  if (mcp.digitalRead(1) == 0) { // Als knop 1 wordt ingedukt
    if (playerTurn == 1) { // Als speler 1 aan de beurt is
      ledGreen[6] = !ledGreen[6]; // Verander de staat van LED 7
      ledGreen[7] = !ledGreen[7]; // Verander de staat van LED 8
      ledGreen[5] = !ledGreen[5]; // Verander de staat van LED 6
      ledGreen[2] = !ledGreen[2]; // Verander de staat van LED 3
      ledGreen[10] = !ledGreen[10]; // Verander de staat van LED 11
    } else if (playerTurn == 2) { // Als speler 2 aan de beurt is
      ledBlue[6] = !ledBlue[6]; // Verander de staat van LED 7
      ledBlue[7] = !ledBlue[7]; // Verander de staat van LED 8
      ledBlue[5] = !ledBlue[5]; // Verander de staat van LED 6
      ledBlue[2] = !ledBlue[2]; // Verander de staat van LED 3
      ledBlue[10] = !ledBlue[10]; // Verander de staat van LED 11
    }

    buttonRead = 0; // Zet buttonRead op 0, waardoor er een stap gezet wordt
  }

  if (mcp.digitalRead(2) == 0) { // Als knop 2 wordt ingedukt
    if (playerTurn == 1) { // Als speler 1 aan de beurt is
      ledGreen[6] = !ledGreen[6]; // Verander de staat van LED 7
      ledGreen[5] = !ledGreen[5]; // Verander de staat van LED 6
      ledBlue[4] = !ledBlue[4]; // Verander de staat van LED 5
      ledGreen[1] = !ledGreen[1]; // Verander de staat van LED 2
      ledGreen[9] = !ledGreen[9]; // Verander de staat van LED 10
    } else if (playerTurn == 2) { // Als speler 2 aan de beurt is
      ledBlue[6] = !ledBlue[6]; // Verander de staat van LED 7
      ledBlue[5] = !ledBlue[5]; // Verander de staat van LED 6
      ledGreen[4] = !ledGreen[4]; // Verander de staat van LED 5
      ledBlue[1] = !ledBlue[1]; // Verander de staat van LED 2
      ledBlue[9] = !ledBlue[9]; // Verander de staat van LED 10
    }

    buttonRead = 0; // Zet buttonRead op 0, waardoor er een stap gezet wordt
  }

  if (mcp.digitalRead(3) == 0) { // Als knop 3 wordt ingedukt
    if (playerTurn == 1) { // Als speler 1 aan de beurt is
      ledGreen[7] = !ledGreen[7]; // Verander de staat van LED 8
      ledGreen[5] = !ledGreen[5]; // Verander de staat van LED 6
      ledBlue[4] = !ledBlue[4]; // Verander de staat van LED 5
      ledGreen[0] = !ledGreen[0]; // Verander de staat van LED 1
      ledGreen[8] = !ledGreen[8]; // Verander de staat van LED 9
    } else if (playerTurn == 2) { // Als speler 2 aan de beurt is
      ledBlue[7] = !ledBlue[7]; // Verander de staat van LED 8
      ledBlue[5] = !ledBlue[5]; // Verander de staat van LED 6
      ledGreen[4] = !ledGreen[4]; // Verander de staat van LED 5
      ledBlue[0] = !ledBlue[0]; // Verander de staat van LED 1
      ledBlue[8] = !ledBlue[8]; // Verander de staat van LED 9
    }

    buttonRead = 0; // Zet buttonRead op 0, waardoor er een stap gezet wordt
  }

  if (mcp.digitalRead(4) == 0) { // Als knop 4 wordt ingedukt
    if (playerTurn == 1) { // Als speler 1 aan de beurt is
      ledGreen[7] = !ledGreen[7]; // Verander de staat van LED 8
      ledGreen[3] = !ledGreen[3]; // Verander de staat van LED 4
      ledGreen[2] = !ledGreen[2]; // Verander de staat van LED 3
      ledGreen[0] = !ledGreen[0]; // Verander de staat van LED 1
      ledGreen[15] = !ledGreen[15]; // Verander de staat van LED 16
    } else if (playerTurn == 2) { // Als speler 2 aan de beurt is
      ledBlue[7] = !ledBlue[7]; // Verander de staat van LED 8
      ledBlue[3] = !ledBlue[3]; // Verander de staat van LED 4
      ledBlue[2] = !ledBlue[2]; // Verander de staat van LED 3
      ledBlue[0] = !ledBlue[0]; // Verander de staat van LED 1
      ledBlue[15] = !ledBlue[15]; // Verander de staat van LED 16
    }

    buttonRead = 0; // Zet buttonRead op 0, waardoor er een stap gezet wordt
  }

  if (mcp.digitalRead(5) == 0) { // Als knop 5 wordt ingedukt
    if (playerTurn == 1) { // Als speler 1 aan de beurt is
      ledGreen[2] = !ledGreen[2]; // Verander de staat van LED 3
      ledGreen[6] = !ledGreen[6]; // Verander de staat van LED 7
      ledGreen[3] = !ledGreen[3]; // Verander de staat van LED 4
      ledGreen[14] = !ledGreen[14]; // Verander de staat van LED 15
      ledGreen[1] = !ledGreen[1]; // Verander de staat van LED 2
    } else if (playerTurn == 2) { // Als speler 2 aan de beurt is
      ledBlue[2] = !ledBlue[2]; // Verander de staat van LED 3
      ledBlue[6] = !ledBlue[6]; // Verander de staat van LED 7
      ledBlue[3] = !ledBlue[3]; // Verander de staat van LED 4
      ledBlue[14] = !ledBlue[14]; // Verander de staat van LED 15
      ledBlue[1] = !ledBlue[1]; // Verander de staat van LED 2
    }

    buttonRead = 0; // Zet buttonRead op 0, waardoor er een stap gezet wordt
  }

  if (mcp.digitalRead(6) == 0) { // Als knop 6 wordt ingedukt
    if (playerTurn == 1) { // Als speler 1 aan de beurt is
      ledGreen[2] = !ledGreen[2]; // Verander de staat van LED 3
      ledGreen[5] = !ledGreen[5]; // Verander de staat van LED 6
      ledGreen[0] = !ledGreen[0]; // Verander de staat van LED 1
      ledGreen[13] = !ledGreen[13]; // Verander de staat van LED 14
      ledGreen[1] = !ledGreen[1]; // Verander de staat van LED 2
    } else if (playerTurn == 2) { // Als speler 2 aan de beurt is
      ledBlue[2] = !ledBlue[2]; // Verander de staat van LED 3
      ledBlue[5] = !ledBlue[5]; // Verander de staat van LED 6
      ledBlue[0] = !ledBlue[0]; // Verander de staat van LED 1
      ledBlue[13] = !ledBlue[13]; // Verander de staat van LED 14
      ledBlue[1] = !ledBlue[1]; // Verander de staat van LED 2
    }

    buttonRead = 0; // Zet buttonRead op 0, waardoor er een stap gezet wordt
  }

  if (mcp.digitalRead(7) == 0) { // Als knop 7 wordt ingedukt
    if (playerTurn == 1) { // Als speler 1 aan de beurt is
      ledBlue[4] = !ledBlue[4]; // Verander de staat van LED 5
      ledGreen[0] = !ledGreen[0]; // Verander de staat van LED 1
      ledGreen[3] = !ledGreen[3]; // Verander de staat van LED 4
      ledGreen[12] = !ledGreen[12]; // Verander de staat van LED 13
      ledGreen[1] = !ledGreen[1]; // Verander de staat van LED 2
    } else if (playerTurn == 2) { // Als speler 2 aan de beurt is
      ledGreen[4] = !ledGreen[4]; // Verander de staat van LED 5
      ledBlue[0] = !ledBlue[0]; // Verander de staat van LED 1
      ledBlue[3] = !ledBlue[3]; // Verander de staat van LED 4
      ledBlue[12] = !ledBlue[12]; // Verander de staat van LED 13
      ledBlue[1] = !ledBlue[1]; // Verander de staat van LED 2
    }

    buttonRead = 0; // Zet buttonRead op 0, waardoor er een stap gezet wordt
  }

  if (mcp.digitalRead(8) == 0) { // Als knop 8 wordt ingedukt
    if (playerTurn == 1) { // Als speler 1 aan de beurt is
      ledGreen[3] = !ledGreen[3]; // Verander de staat van LED 4
      ledGreen[15] = !ledGreen[15]; // Verander de staat van LED 16
      ledGreen[11] = !ledGreen[11]; // Verander de staat van LED 12
      ledGreen[14] = !ledGreen[14]; // Verander de staat van LED 15
      ledGreen[12] = !ledGreen[12]; // Verander de staat van LED 13
    } else if (playerTurn == 2) { // Als speler 2 aan de beurt is
      ledBlue[3] = !ledBlue[3]; // Verander de staat van LED 4
      ledBlue[15] = !ledBlue[15]; // Verander de staat van LED 16
      ledBlue[11] = !ledBlue[11]; // Verander de staat van LED 12
      ledBlue[14] = !ledBlue[14]; // Verander de staat van LED 15
      ledBlue[12] = !ledBlue[12]; // Verander de staat van LED 13
    }

    buttonRead = 0; // Zet buttonRead op 0, waardoor er een stap gezet wordt
  }

  if (mcp.digitalRead(9) == 0) { // Als knop 9 wordt ingedukt
    if (playerTurn == 1) { // Als speler 1 aan de beurt is
      ledGreen[2] = !ledGreen[2]; // Verander de staat van LED 3
      ledGreen[14] = !ledGreen[14]; // Verander de staat van LED 15
      ledGreen[15] = !ledGreen[15]; // Verander de staat van LED 16
      ledGreen[13] = !ledGreen[13]; // Verander de staat van LED 14
      ledGreen[10] = !ledGreen[10]; // Verander de staat van LED 11
    } else if (playerTurn == 2) { // Als speler 2 aan de beurt is
      ledBlue[2] = !ledBlue[2]; // Verander de staat van LED 3
      ledBlue[14] = !ledBlue[14]; // Verander de staat van LED 15
      ledBlue[15] = !ledBlue[15]; // Verander de staat van LED 16
      ledBlue[13] = !ledBlue[13]; // Verander de staat van LED 14
      ledBlue[10] = !ledBlue[10]; // Verander de staat van LED 11
    }

    buttonRead = 0; // Zet buttonRead op 0, waardoor er een stap gezet wordt
  }

  if (mcp.digitalRead(10) == 0) { // Als knop 10 wordt ingedukt
    if (playerTurn == 1) { // Als speler 1 aan de beurt is
      ledGreen[13] = !ledGreen[13]; // Verander de staat van LED 14
      ledGreen[12] = !ledGreen[12]; // Verander de staat van LED 13
      ledGreen[14] = !ledGreen[14]; // Verander de staat van LED 15
      ledGreen[9] = !ledGreen[9]; // Verander de staat van LED 10
      ledGreen[1] = !ledGreen[1]; // Verander de staat van LED 2
    } else if (playerTurn == 2) { // Als speler 2 aan de beurt is
      ledBlue[13] = !ledBlue[13]; // Verander de staat van LED 14
      ledBlue[12] = !ledBlue[12]; // Verander de staat van LED 13
      ledBlue[14] = !ledBlue[14]; // Verander de staat van LED 15
      ledBlue[9] = !ledBlue[9]; // Verander de staat van LED 10
      ledBlue[1] = !ledBlue[1]; // Verander de staat van LED 2
    }

    buttonRead = 0; // Zet buttonRead op 0, waardoor er een stap gezet wordt
  }

  if (mcp.digitalRead(11) == 0) { // Als knop 11 wordt ingedukt
    if (playerTurn == 1) { // Als speler 1 aan de beurt is
      ledGreen[12] = !ledGreen[12]; // Verander de staat van LED 13
      ledGreen[13] = !ledGreen[13]; // Verander de staat van LED 14
      ledGreen[15] = !ledGreen[15]; // Verander de staat van LED 16
      ledGreen[0] = !ledGreen[0]; // Verander de staat van LED 1
      ledGreen[8] = !ledGreen[8]; // Verander de staat van LED 9
    } else if (playerTurn == 2) { // Als speler 2 aan de beurt is
      ledBlue[12] = !ledBlue[12]; // Verander de staat van LED 13
      ledBlue[13] = !ledBlue[13]; // Verander de staat van LED 14
      ledBlue[15] = !ledBlue[15]; // Verander de staat van LED 16
      ledBlue[0] = !ledBlue[0]; // Verander de staat van LED 1
      ledBlue[8] = !ledBlue[8]; // Verander de staat van LED 9
    }

    buttonRead = 0; // Zet buttonRead op 0, waardoor er een stap gezet wordt
  }

  if (mcp.digitalRead(12) == 0) { // Als knop 12 wordt ingedukt
    if (playerTurn == 1) { // Als speler 1 aan de beurt is
      ledGreen[11] = !ledGreen[11]; // Verander de staat van LED 12
      ledGreen[7] = !ledGreen[7]; // Verander de staat van LED 8
      ledGreen[10] = !ledGreen[10]; // Verander de staat van LED 11
      ledGreen[15] = !ledGreen[15]; // Verander de staat van LED 16
      ledGreen[8] = !ledGreen[8]; // Verander de staat van LED 9
    } else if (playerTurn == 2) { // Als speler 2 aan de beurt is
      ledBlue[11] = !ledBlue[11]; // Verander de staat van LED 12
      ledBlue[7] = !ledBlue[7]; // Verander de staat van LED 8
      ledBlue[10] = !ledBlue[10]; // Verander de staat van LED 11
      ledBlue[15] = !ledBlue[15]; // Verander de staat van LED 16
      ledBlue[8] = !ledBlue[8]; // Verander de staat van LED 9
    }

    buttonRead = 0; // Zet buttonRead op 0, waardoor er een stap gezet wordt
  }

  if (mcp.digitalRead(13) == 0) { // Als knop 13 wordt ingedukt
    if (playerTurn == 1) { // Als speler 1 aan de beurt is
      ledGreen[11] = !ledGreen[11]; // Verander de staat van LED 12
      ledGreen[10] = !ledGreen[10]; // Verander de staat van LED 11
      ledGreen[9] = !ledGreen[9]; // Verander de staat van LED 10
      ledGreen[14] = !ledGreen[14]; // Verander de staat van LED 15
      ledGreen[6] = !ledGreen[6]; // Verander de staat van LED 7
    } else if (playerTurn == 2) { // Als speler 2 aan de beurt is
      ledBlue[11] = !ledBlue[11]; // Verander de staat van LED 12
      ledBlue[10] = !ledBlue[10]; // Verander de staat van LED 11
      ledBlue[9] = !ledBlue[9]; // Verander de staat van LED 10
      ledBlue[14] = !ledBlue[14]; // Verander de staat van LED 15
      ledBlue[6] = !ledBlue[6]; // Verander de staat van LED 7
    }

    buttonRead = 0; // Zet buttonRead op 0, waardoor er een stap gezet wordt
  }

  if (mcp.digitalRead(14) == 0) { // Als knop 14 wordt ingedukt
    if (playerTurn == 1) { // Als speler 1 aan de beurt is
      ledGreen[9] = !ledGreen[9]; // Verander de staat van LED 10
      ledGreen[8] = !ledGreen[8]; // Verander de staat van LED 9
      ledGreen[10] = !ledGreen[10]; // Verander de staat van LED 11
      ledGreen[13] = !ledGreen[13]; // Verander de staat van LED 14
      ledGreen[5] = !ledGreen[5]; // Verander de staat van LED 6
    } else if (playerTurn == 2) { // Als speler 2 aan de beurt is
      ledBlue[9] = !ledBlue[9]; // Verander de staat van LED 10
      ledBlue[8] = !ledBlue[8]; // Verander de staat van LED 9
      ledBlue[10] = !ledBlue[10]; // Verander de staat van LED 11
      ledBlue[13] = !ledBlue[13]; // Verander de staat van LED 14
      ledBlue[5] = !ledBlue[5]; // Verander de staat van LED 6
    }

    buttonRead = 0; // Zet buttonRead op 0, waardoor er een stap gezet wordt
  }

  if (mcp.digitalRead(15) == 0) { // Als knop 15 wordt ingedukt
    while (mcp.digitalRead(15) == 0) {
      delay(1000);
      if (mcp.digitalRead(15) == 0) { // Als de knop nog steeds ingedrukt is na de delay
        if (counter >= 3) { // Als de counter hoger of gelijk is aan 5 seconde
         pauseState = true;
          pauseGame();
        } else { // anders
        Serial.println("Counter++");
          counter++; // counter = counter + 1
        }
      } else {
        Serial.println("Knop niet meer ingedrukt na delay");
      }
    }
    counter = 0;

    if (playerTurn == 1) { // Als speler 1 aan de beurt is
      ledBlue[4] = !ledBlue[4]; // Verander de staat van LED 5
      ledGreen[8] = !ledGreen[8]; // Verander de staat van LED 9
      ledGreen[12] = !ledGreen[12]; // Verander de staat van LED 13
      ledGreen[11] = !ledGreen[11]; // Verander de staat van LED 12
      ledGreen[9] = !ledGreen[9]; // Verander de staat van LED 10
    } else if (playerTurn == 2) { // Als speler 2 aan de beurt is
      ledGreen[4] = !ledGreen[4]; // Verander de staat van LED 5
      ledBlue[8] = !ledBlue[8]; // Verander de staat van LED 9
      ledBlue[12] = !ledBlue[12]; // Verander de staat van LED 13
      ledBlue[11] = !ledBlue[11]; // Verander de staat van LED 12
      ledBlue[9] = !ledBlue[9]; // Verander de staat van LED 10
    }

    buttonRead = 0; // Zet buttonRead op 0, waardoor er een stap gezet wordt
  }


  if (buttonRead == 0) { // Als er een knop is ingedrukt
    if (gameOn) { // en als het spel gestart is
      ledSteps++; // doe het aantal stappen + 1
    }
    buttonRead = 99; // Zet gelezene button terug naar default
    if (players == 2) { // als er multiplayer is
      if (greenWon) { // heeft groen gewonnen?
        playerTurn = 1; // alleen speler 1 aan de beurt
      } else if (blueWon) { // heeft speler 2 gewonnen
        playerTurn = 2; // zet speler 1 aan de beurt
      } else if (playerTurn == 1) { // Als speler 1 aan de beurt is
        playerTurn = 2; // speler 2 is de volgende zet aan de beurt
      } else { // anders
        playerTurn = 1; // speler 1 is de volgende zet aan de beurt
      }
    }
    setLed(); // ga naar de functie setLed()
  }
}

void pauseGame() {
  T.PauseTimer();
  counter = 0;
  clearLeds();
  while (pauseState) {
    while (mcp.digitalRead(15) == 0) {
      if (T.TimeHasChanged()) {
        if (counter <= 3) {
          pauseState = false;
          T.ResumeTimer();
          setLed();
        } else if(counter > 3) {
          
          for(int i = 0; i < 16; i++){
          ledGreen[i] = 0;
          ledBlue[i] = 0;
          }
          
          gameOn = false;
          players = 0;
          startGame();

        }
        counter++;
      }
    }
    counter = 0;
  }
}

void setLed() { // setLed functie om de leds te besturen. Authors: Duncan en Dennis
  // Bit shift de array in een byte formaat voor beide rijen en 2 kleuren
  char boe = (ledGreen[0] << 7) | (ledGreen[1] << 6) | (ledGreen[2] << 5) | (ledGreen[3] << 4) | (ledGreen[4] << 3) | (ledGreen[5] << 2) | (ledGreen[6] << 1) |  (ledGreen[7] << 0);
  char boetwee = (ledGreen[8] << 7) | (ledGreen[9] << 6) | (ledGreen[10] << 5) | (ledGreen[11] << 4) | (ledGreen[12] << 3) | (ledGreen[13] << 2) | (ledGreen[14] << 1) |  (ledGreen[15] << 0);
  char boo = (ledBlue[0] << 7) | (ledBlue[1] << 6) | (ledBlue[2] << 5) | (ledBlue[3] << 4) | (ledBlue[4] << 3) | (ledBlue[5] << 2) | (ledBlue[6] << 1) |  (ledBlue[7] << 0);
  char bootwo = (ledBlue[8] << 7) | (ledBlue[9] << 6) | (ledBlue[10] << 5) | (ledBlue[11] << 4) | (ledBlue[12] << 3) | (ledBlue[13] << 2) | (ledBlue[14] << 1) |  (ledBlue[15] << 0);

  // Zet de  LEDs
  lc.setColumn(3, 2, boetwee);
  lc.setColumn(3, 3, bootwo);
  lc.setColumn(3, 6, boo);
  lc.setColumn(3, 5, boe);

  delay(300); // wacht 300 miliseconden

}

// Toont de huidige time die bijgehouden is door de Arduino op het aangegeven segment (int s) Author: Dennis
void showTime(int s)
{
  if (T.TimeHasChanged()) {
    showSec = T.ShowSeconds();
    showMin = T.ShowMinutes();

    leftTimeSec = showSec / 10;
    rightTimeSec = showSec % 10;
    leftTimeMin = showMin / 10;
    rightTimeMin = showMin % 10;

    doublePoints = !doublePoints;

    lc.setDigit(s, 0, leftTimeMin, false);
    lc.setDigit(s, 5, rightTimeMin, false);
    lc.setDigit(s, 2, leftTimeSec, false);
    lc.setDigit(s, 3, rightTimeSec, doublePoints);
  }
}

// Toon de score (global int score) van de huidige game op het aangegeven segment (int s) Author: Dennis
void showScore(int s)
{
  s1 = (score / 1000);
  s2 = ((score / 100) % 10);
  s3 = ((score / 10) % 10);
  s4 = (score % 10);

  lc.setDigit(s, 0, s1, false);
  lc.setDigit(s, 5, s2, false);
  lc.setDigit(s, 2, s3, false);
  lc.setDigit(s, 3, s4, false);
}

// Toon het aantal stappen die de speler heeft gedaan Author: Dennis
void showSteps(int s)
{
  s1 = (ledSteps / 1000);
  s2 = ((ledSteps / 100) % 10);
  s3 = ((ledSteps / 10) % 10);
  s4 = (ledSteps % 10);

  // boven-rechtsboven-rechtsonder-onder-linksonder-linksboven-middenstreepje
  lc.setChar(s, 0, 5, false);
  lc.setRow(s , 1, B10001111); // TOON EEN T
  lc.setChar(s, 2, 'e', false);
  lc.setChar(s, 3, 'p', false);

  lc.setDigit(s, 4, s1, false);
  lc.setDigit(s, 5, s2, false);
  lc.setDigit(s, 6, s3, false);
  lc.setDigit(s, 7, s4, false);
}

// Toont huidige level (global int level) van de huidige game op het aangegeven segment (int s) Author: Dennis
void showLevel(int s)
{
  lc.setChar(s, 0, 'A', false);
  lc.setRow(s , 1, B01000110); // TOON EEN R
  lc.setChar(s, 2, 'E', false);
  lc.setChar(s, 3, 'A', false);

  lc.setDigit(s, 4, level + 1, false); // +1 want level begint bij 0 intern
  lc.setChar(s, 5, '-', false);
  lc.setChar(s, 6, '-', false);
  lc.setDigit(s, 7, levels, false);
}

// Toont bericht dat het huidige level succesvol is afgesloten en het nieuwe level eraan komt
void showCompleted()
{
  lc.setChar(1, 0, 'G', false);
  lc.setChar(1, 1, 0, false);
  lc.setChar(1, 2, 0, false);
  lc.setChar(1, 3, 'd', false);
  lc.setChar(1, 4, ' ', false);
  lc.setChar(1, 5, 'j', false);
  lc.setChar(1, 6, 0, false);
  lc.setChar(1, 7, 'b', false);

  lc.setChar(0, 0, 'n', false);
  lc.setChar(0, 1, 'e', false);
  lc.setChar(0, 2, 'x', false);
  lc.setChar(0, 3, 't', false);
  lc.setChar(0, 4, ' ', false);
  lc.setChar(0, 5, 'l', false);
  lc.setChar(0, 6, 'v', false);
  lc.setChar(0, 7, 'l', false);
}

// Toont start bericht waarin de speler een keuze moet maken tussen 1 of 2 spelers Author: Dennis
void showStart()
{
  if (!gameOn) {
    lc.setRow(0 , 0, B01001110); // TOON EEN C
    lc.setChar(0, 1, 'H', false);
    lc.setChar(0, 2, '0', false);
    lc.setChar(0, 3, '0', false);
    lc.setChar(0, 4, '5', false);
    lc.setChar(0, 5, 'E', false);
    lc.setChar(0, 6, ' ', false);
    lc.setChar(0, 7, ' ', false);

    lc.setChar(1, 0, 'P', false);
    lc.setChar(1, 1, 'L', false);
    lc.setChar(1, 2, 'A', false);
    lc.setChar(1, 3, '4', false);
    lc.setChar(1, 4, 'E', false);
    lc.setRow(1 , 5, B01000110); // TOON EEN R
    lc.setChar(1, 6, 5, false);
    lc.setChar(1, 7, ' ', false);

    lc.setChar(2, 0, '1', false);
    lc.setChar(2, 5, '0', false);
    lc.setRow(2 , 2, B01000110); // TOON EEN R
    lc.setChar(2, 3, '2', false);
  }
}

void fallingLeds() { // Falling LEd's functie, animatie als er een level gewonnen is. Author: duncan

  for (int i = 0; i < 41; i++) { // Loop door de array heen, waar het patroon in staat
    if ((i % 2) == 0) {  // kijk of het getal even is
      // zet de bovenste rijen aan
      lc.setColumn(3, 4, fallingLedsArray[i]);
      lc.setColumn(3, 5, fallingLedsArray[i]);
      lc.setColumn(3, 6, fallingLedsArray[i]);
    } else { // als het getal oneven is
      // zet de onderste rijen aan
      lc.setColumn(3, 1, fallingLedsArray[i]);
      lc.setColumn(3, 2, fallingLedsArray[i]);
      lc.setColumn(3, 3, fallingLedsArray[i]);
      delay(50); // wacht 10 miliseconde
    }
  }

  clearLeds(); // Zet alle leds uit

}
// Zet alle leds uit Author: Dennis
void clearLeds()
{
  for (int i = 1; i < 7; i++) // loop door alle rijen heen
  {
    lc.setColumn(3, i, B00000000); // zet de hele rij uit
  }

}

// Deze functie wordt aangeroepen vanuit hoofd menu als een game gestart word Author: Dennis
void startUp() {
  nextLevel();
  setLed();
  clearScreen();
}

// Zet alles segmenten op de 7-segment display uit Author: Dennis
void clearScreen() {
  if (clearScreenState == true )
  {
    for (int i = 0; i < 3; i++) {
      for (int y = 0; y < 8; y++) {
        lc.setChar(i, y, ' ', false);
      }
    }
    clearScreenState = false;
  }
}

void startGame() { // eerste functie als het spel opgestart wordt. Author: Duncan, Charlie en Dennis
  // Zet 3 leds aan. 1 voor 1 speler. 2 voor 2 spelers
  lc.setColumn(3, 4, B01000110);
  lc.setColumn(3, 5, B01000110);
  lc.setColumn(3, 6, B01000110);

  if (mcp.digitalRead(1) == 0) {
    players = 1;
  } else if (mcp.digitalRead(2) == 0) {
    players = 2;
  }
  if (players != 0) {
    clearLeds();
    gameOn = true;
    clearScreenState = true;
    startUp();
    delay(300);
  }
}

void play_rtttl(char *p) // Functie voor het spelen van geluid. Author: Duncan
{

  byte default_dur = 4; // de kleinste waarde die nodig is om goed geluid te krijgen
  byte default_oct = 6; // Beste toon voor de meeste speakers
  int bpm = 63; // Beats per minute. Snelheid waarmee het liedje wordt afgespeeld
  int num; // welk nummer
  long wholenote; // De noot
  long duration; // long voor hoe lang het lied is
  byte note; // byte voor de note
  byte scale; // hoe hard

  while (*p != ':') p++; // Sla de naam over
  p++; // Sla de : over

  // Haal de duration op van het liedje
  if (*p == 'd')
  {
    p++; p++; // Sla de "d=" over
    num = 0; // Reset de num
    while (isdigit(*p)) // Zolang p een nummer is
    {
      num = (num * 10) + (*p++ - '0'); // tel hoeveel nummers er zijn, behalve 0
    }
    if (num > 0) default_dur = num; // als nummer langer is dan de default, neem het over
    p++; // Sla de komma over
  }

  // Haal de octave op
  if (*p == 'o') // kijk als p gelijk is aan 0
  {
    p++; p++; // Sla de "o=" over
    num = *p++ - '0'; // neem p + 1 over, behalve als het 0 is
    if (num >= 3 && num <= 7) default_oct = num; // kijk of num binnen 4, 5, 6 valt
    p++; // Sla de komma over
  }

  // Haal de bpm op
  if (*p == 'b') // als p gelijk is aan b
  {
    p++; p++; // Sla de "b=" over
    num = 0; // reset num
    while (isdigit(*p)) // zolang p een nummer is
    {
      num = (num * 10) + (*p++ - '0'); // tel het getal bij elkaar op
    }
    bpm = num; // neem bpm over
    p++; // Sla de dubbele punt over
  }

  // BPM betekent dat er een 1/4 note per minute wordt gespeeld
  wholenote = (60 * 1000L / bpm) * 4;  // de tijd voor een hele note in milisecondes

  // loop door rest van liedje heen
  while (*p)
  {
    // haal de note duration op
    num = 0; // reset num
    while (isdigit(*p)) // zolang p een nummer is
    {
      num = (num * 10) + (*p++ - '0'); // tel de nummers bij elkaar op
    }

    if (num) duration = wholenote / num; // tijd van een hele noot gedeelte door de tijd van 1 note
    else duration = wholenote / default_dur;  // anders tijd van een hele noot gedeeld door de default duration

    // haal de note op
    note = 0;
    // Kijk welke note er gespeeld wordt
    switch (*p)
    {
      case 'c':
        note = 1;
        break;
      case 'd':
        note = 3;
        break;
      case 'e':
        note = 5;
        break;
      case 'f':
        note = 6;
        break;
      case 'g':
        note = 8;
        break;
      case 'a':
        note = 10;
        break;
      case 'b':
        note = 12;
        break;
      case 'p':
      default:
        note = 0;
    }
    p++; // p = p + 1

    // kijk als er een kruis is  (hoger/ sharp)
    if (*p == '#')
    {
      note++; // note = note + 1
      p++; // p = p + 1
    }

    // Kijk als er een mol is (laag)
    if (*p == '.')
    {
      duration += duration / 2; // duration wordt gedeeld door 2, waardoor lager klinkt
      p++; // p = p + 1
    }

    // kijk naar volume
    if (isdigit(*p)) // als p een nummer is
    {
      scale = *p - '0'; // scale is p, behalve als hij 0 is
      p++; // p = p + 1
    }
    else // anders
    {
      scale = default_oct; // scale is normale octave
    }

    scale += OCTAVE_OFFSET; // verhoog het volume

    if (*p == ',') // kijk als er een komma is
      p++; // p = p + 1, skip de komma of einde van liedje

    // speel de noot
    if (note) // als er een note is
    {
      tone(TONE_PIN, notes[(scale - 4) * 12 + note]); // roep de arduino Tone functie aan, met alle juiste waardes
      delay(duration); // wacht de duration
      noTone(TONE_PIN); // stop de noot
    }
    else // anders
    {
      delay(duration); // wacht op de volgende noot
    }
  }
}

void checkWin() // functie voor het checken als er gewonnen is. Author: Duncan en Dennis
{
  won = 1;

  for (int i = 0; i < 16; i++)
  {
    if (players == 1) {
      if (ledGreen[i]) {
        won = 0;
        return;
      }
    } else if (players == 2) { // als er 2 spelers zijn
      if (ledGreen[i]) { // kijk of er nog groene leds zijn
        greenWon = false; // ZO ja, neit gewonnen
        won = 0;
      }

      if (ledBlue[i]) { // kijk als er een blauwe leds zijn
        blueWon = false; // zo ja, niet gewonnen
        won = 0;
      }
    }
  }

  if (blueWon || greenWon) { // als 1 van de 2 spelers gewonnen heeft
    won = 1; // gewonnen
  }

  if (won) {
    seconds  = 0;
    minutes = 0;
    //showCompleted();
    score = ledSteps + T.ShowSeconds() + (T.ShowMinutes() * 60);
    showScore(2);
    char *song = "victory-fa:d=4,o=5,b=150:p,c,16c6,16p,16g,16p,16c6,16p,d#.6,16f6,16p,16f6,16p,16f6,16p,1g6"; // Liedje wat gespeeld wordt.
    play_rtttl(song); // speel liedje
    fallingLeds();
    seconds = T.ShowSeconds();
    minutes = T.ShowMinutes();
    checkScore(seconds, minutes, ledSteps);
    T.ResetTimer();
    ledSteps = 0;
    level++;
  }
}

void checkScore(int seconds, int minutes, int steps) { // Functie voor de highscore. Author: duncan
  eepromreader = 0; // eeprom lezer
  eepromreadermax = 0; // eeprom max lezer

  // Er is nog geen highscore neer gezet
  stepScore = false;
  secondScore = false;
  minuteScore = false;

  int stepArray, secondArray, minuteArray; // welk nummer gelezen wordt

  // Als level 1 is:
  if (level == 0) {
    eepromreader = 0; // lezer op 0
    eepromreadermax = eepromreader + 2; // max is lezer + 2

    stepArray = 0; // stap is memory adres 1
    secondArray = 1; // secondes is memory adres 2
    minuteArray = 2; // minuten is memory adres 3

  } else if (level == 1) { // anders als level 2
    eepromreader = 3; // lezer op 3
    eepromreadermax = eepromreader + 2; // max is lezer + 2

    stepArray = 3; // stap is memory adres 3
    secondArray = 4; // secondes is memory adres 4
    minuteArray = 5; // minuten is memory adres 5

  } else if (level == 2) { // anders als level 3
    eepromreader = 6; // lezer op 6
    eepromreadermax = eepromreader + 2; // max is lezer + 2

    stepArray = 6; // stap is memory adres 6
    secondArray = 7; // secondes is memory adres 7
    minuteArray = 8; // minuten is memory adres 8

  } else if (level == 3) { // anders als level 4
    eepromreader = 9; // lezer op 9
    eepromreadermax = eepromreader + 2; // max is lezer + 2

    stepArray = 9; // stap is memory adres 9
    secondArray = 10; // secondes is memory adres 10
    minuteArray = 11; // minuten is memory adres 11

  } else if (level == 4) { // anders als level 5
    eepromreader = 12; // lezer op 12
    eepromreadermax = eepromreader + 2; // max is lezer + 2

    stepArray = 12; // stap is memory adres 12
    secondArray = 13; // secondes is memory adres 13
    minuteArray = 14; // minuten is memory adres 14
  }

  // loop door het geheugen heen, vanaf het eerste adres van het level
  for (int i = eepromreader; i <= eepromreadermax; i++) {
    oldScore[i] = EEPROM.read(i); // haal oude stappen op
    oldScore[i] = EEPROM.read(i); // haal oude seconden op
    oldScore[i] = EEPROM.read(i); // haal oude minuten op
  }

  // als oude stappen hoger of gelijk is aan huidige stappen
  if (oldScore[stepArray] >= steps) {
    stepScore = true; // stappen is een high score
  }

  // als oude minuten hoger of gelijk is aan huidige minuten
  if (oldScore[minuteArray] >= minutes) {
    minuteScore = true; // minuten is een high score
  }

  // als oude seconden hoger is aan huidige minuten
  if (oldScore[secondArray] > seconds) {
    secondScore = true; // seconden is een high score
  }

  // als alle drie een high score is
  if (stepScore && minuteScore && secondScore) {
    // schrijf de nieuwe high score in het geheugen
    EEPROM.write(stepArray, ledSteps);
    EEPROM.write(secondArray, seconds);
    EEPROM.write(minuteArray, minutes);

    // speel een muziekje
    char *song = "smb1-1up:d=4,o=5,b=220:16e7,16p,16g7,16p,16e8,16p,16c8,16p,16d8,16p,2g8"; // Liedje wat gespeeld wordt.

    // LED animatie
    for (int i = 0; i < 13; i++) { // Loop door de array heen, waar het patroon in staat
      if ((i % 2) == 0) {  // kijk of het getal even is
        // zet de bovenste rijen aan
        lc.setColumn(3, 4, starLedsArray[i]);
        lc.setColumn(3, 5, starLedsArray[i]);
        lc.setColumn(3, 6, starLedsArray[i]);
      } else { // als het getal oneven is
        // zet de onderste rijen aan
        lc.setColumn(3, 1, starLedsArray[i]);
        lc.setColumn(3, 2, starLedsArray[i]);
        lc.setColumn(3, 3, starLedsArray[i]);
        delay(250); // wacht 250 miliseconde
      }
    }

    // speel het liedje
    play_rtttl(song);
  } else { // anders
    // reset de high score
    stepScore = false;
    minuteScore = false;
    secondScore = false;
  }
}


// Laad de volgende level vanuit de levels array in de huidige game arrays Author: Dennis
void nextLevel()
{
  clearLeds();
  for (int i = 0; i < 16; i++)
  {
    if (gameLevels[level][i] != 99)
    {
      if (gameLevels[level][i] == 4) {
        if (players == 1) {
          ledBlue[gameLevels[level][i]] = 1;
        } else {
          ledGreen[gameLevels[level][i]] = 1;
          ledBlue[gameLevels[level][i]] = 1;
        }
      } else {
        if (players == 1) {
          ledGreen[gameLevels[level][i]] = 1;
        } else {
          ledGreen[gameLevels[level][i]] = 1;
          ledBlue[gameLevels[level][i]] = 1;
        }
      }
    }
  }

  setLed();
}

// De game loop die altijd blijft lopen Author: Dennis en Duncan
void loop() {
  
  // Als de game gestart is
  if (gameOn)
  {
    T.Timer();
    clearScreen();
    readButtons();
    showTime(2);
    showLevel(1);
    showSteps(0);
    checkWin();
    
    // Als gewonnen ga dan naar het volgende level
    if (won)
    {
      nextLevel();
      won = 0;
    }
    // Anders toon het hoofdmenu
  } else {
    startGame();
    showStart();
  }
}
